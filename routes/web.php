<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    return view('welcome');
});
Route::post('/',function (){
    $query = Request::input('search');

    $students = \App\Student::where('stud_name', 'LIKE', '%' . $query . '%')->get();

    var_dump('search result');

    foreach ($students as $student){
        var_dump($student->stud_name);
    }
});

Route::put('/', function (){

});



Auth::routes();

Route::resource('students','StudentController');
Route::resource('books','BookController');
Route::resource('borrows','BorrowController');
Route::get('borrows/{borrow}/extend','BorrowController@extend')->name('borrows.extend');
Route::put('borrows/{borrow}','BorrowController@returned')->name('borrows.returned');
Route::get('getname/{borrow}', 'BorrowController@getname');
Route::resource('returns','ReturnController');
Route::resource('extends','ExtendController');
Route::get('/home', 'HomeController@index')->name('home');