<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Borrow extends Model
{
    protected $fillable = ['student_id','b_id','stud_name','b_name','b_remark','date_borrowed','due_date','date_returned'];

    public function student() {
        return $this->hasOne('App\Student','id');
    }

    public function book() {
        return $this->hasOne('App\Book','student_id','id');
    }
}
