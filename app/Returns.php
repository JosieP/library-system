<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Returns extends Model
{
    protected $fillable = ['date_returned','student_id','isbn','stud_name','b_name'];
}
