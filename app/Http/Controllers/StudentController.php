<?php

namespace App\Http\Controllers;

use App\Year;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
use UxWeb\SweetAlert;
use App\Book;
use App\Http\Requests\StudentRequest;
use App\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StudentController extends Controller


{
    public function __construct(Student $students)
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Student::select('student_id', DB::raw('CONCAT(firstname, " ", lastname) AS name, id, gender, year'))->get();
        return view('students.index',compact('students'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('students.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StudentRequest $request)
    {
        $students = Student::create($request->all());
        /*$year = new Year;
        $year->year = Input::get('year');
        $students->year()->save($year);*/
        return redirect()->route('students.index')->with('message','Student has been added succesfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Res ponse
     */
    public function show( Student $student, Book $books)
    {
        //$books = DB::table('books')->get();
        //$books = \App\Book::find($student->books);
        //$books->update(($request->input('student_id')));
        //$student = Student::select('student_id', DB::raw('CONCAT(firstname, " ", lastname) AS name, id'))->get();
        $books = \App\Book::where('student_id','=',$student->student_id)->get();
        return view('students.show',compact('student','books'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        return view('students.edit',compact('student'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StudentRequest $request, Student $student)
    {

        $student->update(($request->all()));
        return redirect()->route('students.index')->with('message','Student has been updated succesfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        $student->delete();
        return redirect()->route('students.index')->with('message','Student has been deleted succesfully');
    }


}
