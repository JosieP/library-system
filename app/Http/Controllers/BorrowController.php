<?php

namespace App\Http\Controllers;

use App\Book;
use App\Borrow;
use App\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Input;
use Illuminate\View\View;
use Illuminate\Support\Facades\DB;

class BorrowController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $borrows = Borrow::where('date_returned','=',null)->orderby('date_borrowed','DESC')->get();
        return view('borrows.index', compact('borrows'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Student $students,Book $borrows)
    {
        $students = Student::select('student_id', DB::raw('CONCAT(firstname, " ", lastname) AS name, student_id, id'))->get();
        $borrows = \App\Book::where('student_id','=',null)->get();
        return view('borrows.create',compact('borrows','students'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $borrows = Borrow::create($request->all());
        $students = Student::select('student_id', DB::raw('CONCAT(firstname, " ", lastname) AS name, student_id, id'))->get();
        $books = Book::all();
        /*$stud_id = new Book;
        $stud_id->student_id = Input::get('student_id');
        $borrows->books()->save($stud_id);*/
        foreach ($students as $student)
            foreach ($books as $book)
            DB::table('books')->where('isbn','=',$borrows->b_id)->update(['student_id' => $borrows->student_id]);
        return redirect()->route('borrows.index')->with('message','Student has been added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Borrow $borrow, Student $students)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Borrow $borrow)
    {
        //$students = \App\Book::where('student_id','=',$borrows->id)->get();
        return view('borrows.edit',compact('borrow'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Borrow $borrow)
    {
        $borrow->update(($request->all()));
        //DB::table('books')->where('isbn','=',$borrow->b_id)->update(['student_id' => null]);
        return redirect()->route('borrows.index')->with('message','Borrower has been extend due date');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function extend(Borrow $borrow)
    {

        return view('borrows.extend', compact('borrow'));
    }

    public function returned(Request $request, Borrow $borrow)
    {
        $borrow->update(($request->all()));
        DB::table('books')->where('isbn','=',$borrow->b_id)->update(['student_id' => null]);
        return redirect()->route('borrows.index')->with('message','Books has been returned successfully');
    }

    public function extended(Borrow $borrow)
    {

    }

}
