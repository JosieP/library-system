<?php

namespace App\Http\Controllers;

use App\Book;
use App\Borrow;
use App\Returns;
use App\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReturnController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $returns = Borrow::where('date_returned','!=',null)->orderby('date_returned','DESC')->get();
        return view('returns.index',compact('returns'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $students = Student::all();
        $books = Book::all();
        //$student = Student::select('student_id', DB::raw('CONCAT(firstname, " ", lastname) AS name, id, gender, year, student_id'))->get();
        return view('returns.create', compact('students','books'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $returns = new Returns();
        $returns['date_returned'] = $request->input('date_returned');
        $returns['student_id'] = $request->input('student_id');
        $returns['isbn'] = $request->input('isbn');
        $returns['stud_name'] = $request->input('stud_name');
        $returns['b_name'] = $request->input('b_name');

        $returns->save();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Book $borrow)
    {
        $borrow->update(($request->all()));
        return redirect()->route('borrows.index')->with('message','Borrower has been extend due date');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
