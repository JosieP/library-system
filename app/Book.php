<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = ['isbn','b_name','b_des','b_aut','b_year','student_id'];

    public function student(){
        return $this->belongsTo('App\Student','student_id','id');
    }

    public function borrow(){
        return $this->belongsTo('App\Borrow','student_id','id');
    }
}
