<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = ['firstname','lastname','student_id','gender','year'];

    public function books() {
        return $this->hasMany('App\Book','student_id','id');
    }
    public function borrow() {
        return $this->belongsTo('App\Borrow','id');
    }

   /* public function year() {
      return $this->hasOne('App\Year','student_id');
}*/

}
