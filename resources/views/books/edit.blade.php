@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Update books</div>

                    <div class="panel-body">
                        {!! Form::model($book,array('route'=>['books.update',$book->id],'method'=>'PUT')) !!}
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('isbn','Enter book code') !!}
                                    {!! Form::text('isbn',null,['class'=>'form-control']) !!}
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('b_name','Enter book name') !!}
                                    {!! Form::text('b_name',null,['class'=>'form-control']) !!}
                                </div>
                            </div>

                        <div class="col-xs-12">
                                <div class="form-group">
                                {!! Form::label('b_des','Enter book description') !!}
                                {!! Form::text('b_des',null,['class'=>'form-control']) !!}
                            </div>
                        </div>

                        <div class="col-md-8">
                            <div class="form-group">
                                {!! Form::label('b_aut','Enter book author') !!}
                                {!! Form::text('b_aut',null,['class'=>'form-control']) !!}
                                </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                {!! Form::label('b_year','Enter book release') !!}
                                {!! Form::date('b_year',null,['class'=>'form-control']) !!}
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {!! Form::button('Update book',['type'=>'submit','class'=>'btn btn-primary']) !!}
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                @if($errors->all())
                    <ul class="alert alert-danger">
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
            </div>
        </div>
    </div>
@endsection
