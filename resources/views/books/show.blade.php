@extends('layouts.app')


@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <div class="panel panel-default">

                    <div class="panel-heading">Book Details</div>

                    <div class="panel-body">
                        <div class="col-md-8"></div>
                        <div class="col-md-4">
                        {!! Form::open(array('route'=>'books.store')) !!}
                        Book Code : {{ $book->b_code }}<br>
                        Book Name : {{ $book->b_name }}<br>
                        Book : Description : {{ $book->b_des }}<br>
                        Book author : {{ $book->b_aut }}<br>
                        Book release : {{ $book->b_year }}<br>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
