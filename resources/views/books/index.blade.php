@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                @if(Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif

                <div class="panel panel-default">

                    <div class="panel-heading">Books</div><br>
                    <div class="col-md-8">
                        <div class="form-group">
                            {{ link_to_route('books.create','Add new book',null,['class'=>'btn btn-primary']) }}
                        </div>
                    </div>

                    <div class="panel-body">

                        <table class="table">
                            <tr>
                                <th>Book Code</th>
                                <th>Book Name</th>
                                <th>Book Description</th>
                                <th>Book Author</th>
                                <th>Book Release</th>
                                <th>Action</th>
                            </tr>
                            @foreach($books as $book)
                            <tr>
                                <td>{{ link_to_route('books.show',$book->isbn,$book->id) }}</td>
                                <td>{{ link_to_route('books.show',$book->b_name,$book->id) }}</td>
                                <td>{{ link_to_route('books.show',$book->b_des,$book->id) }}</td>
                                <td>{{ link_to_route('books.show',$book->b_aut,$book->id) }}</td>
                                <td>{{ link_to_route('books.show',$book->b_year,$book->id) }}</td>
                                <td>
                                    {!! Form::open(array('route'=>['books.destroy',$book->id],'method'=>'DELETE')) !!}
                                        {{ link_to_route('books.edit','Edit',[$book->id],['class'=>'btn btn-primary']) }}
                                        |
                                        {!! Form::button('Delete',['class'=>'btn btn-danger','type'=>'submit']) !!}
                                    {!!  Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
