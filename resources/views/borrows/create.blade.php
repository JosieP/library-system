@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">

                    <div class="panel-heading">Add Borrower</div><br>

                    <div class="panel-body">
                        {!! Form::open(array('route'=>'borrows.store')) !!}

                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('student_id','Select Student Name') !!}<br>
                                    <select id="student_id" name="student_id" class="form-control" onchange = "getStudent(this);">
                                        <div class="dropdown">
                                            <option value="" disabled="disabled" selected="selected">Select Student Name</option>
                                            @foreach($students as $student)
                                                    <option id="student_id"  name="student_id" value="{{ $student->student_id }}" >{{ $student->name }}
                                                    </option>

                                            @endforeach
                                        </div>

                                    </select>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('b_code','Select Book Name') !!}<br>
                                    <select id="b_id" name="b_id" class="form-control" onchange = "getBook(this);">
                                        <div class="dropdown">
                                            <option value="" disabled="disabled" selected="selected">Select Book Name</option>
                                            @foreach($borrows as $borrow)
                                                <option id="b_id" name="b_id" value="{{  $borrow->isbn }}">{{ $borrow->b_name }}</option>
                                            @endforeach
                                        </div>

                                    </select>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('stud_name','Student Name') !!}
                                    {!! Form::text('stud_name', null , ['id'=>'stud_name', 'class'=>'form-control', 'readonly']) !!}
                                </div>
                            </div>



                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('b_name','Book Name') !!}
                                    {!! Form::text('b_name',null,['class'=>'form-control','readonly']) !!}
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('date_borrowed','Enter Date Borrowed') !!}
                                    {!! Form::date('date_borrowed', old('date_borrowed',  Carbon\Carbon::today()->format('Y-m-d')), ['class'=>'form-control date-picker', 'readonly']) !!}
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('due_date','Enter Date Return') !!}
                                    {!! Form::date('due_date',null,['class'=>'form-control']) !!}
                                </div>
                            </div>

                            <div class="col-xs-12">
                                <div class="form-group">
                                    {!! Form::label('b_remark','Remarks') !!}
                                    {!! Form::textarea('b_remark',null,['class'=>'form-control','size' => '30x5']) !!}
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    {!! Form::button('Add borrower',['type'=>'submit','class'=>'btn btn-primary']) !!}
                                </div>
                            </div>


                        {!! Form::close() !!}
                        <script>
                            function getStudent(student_id) {
                                /*var selectedText = ddlFruits.options[ddlFruits.selectedIndex].innerHTML;
                                var selectedValue = ddlFruits.value;
                                alert("Selected Text: " + selectedText + " Value: " + selectedValue);*/
                                //var x = student_id.value;

                                var y = student_id.options[ student_id.selectedIndex ].text;
                                document.getElementById("stud_name").value = y;
                                //var txt =  document.getElementById('stud_name').value="{{ $student->name }}";
                                //console.log(seIndex);
                            }
                            //getStudent();

                            function getBook(b_id) {
                                //document.getElementById('b_name').innerHTML = b_id.value;
                                //var seIndex = document.getElementById("stud_id").value;
                                //document.getElementById("b_name").value = data.value;
                                //var a = document.getElementById("b_id").value;
                                var y = b_id.options[ b_id.selectedIndex ].text;
                                document.getElementById("b_name").value = y;
                                //console.log(seIndex);
                            }
                            //getSelectValue();



                        </script>

                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection


