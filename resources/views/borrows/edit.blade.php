@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                @if(Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif

                <div class="panel panel-default">

                    <div class="panel-heading">Return Student</div><br>


                    <div class="panel-body">
                        {!! Form::model($borrow,array('route'=>['borrows.returned',$borrow->id],'method'=>'PUT')) !!}
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('student_id','Student ID') !!}
                                {!! Form::text('student_id', null ,['class'=>'form-control','readonly']) !!}
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('stud_name','Student Name') !!}
                                {!! Form::text('stud_name', null ,['class'=>'form-control','readonly']) !!}
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('b_id','ISBN') !!}
                                {!! Form::text('b_id', null ,['class'=>'form-control','readonly']) !!}
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('b_name','Book Name') !!}
                                {!! Form::text('b_name', null ,['class'=>'form-control','readonly']) !!}
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('date_borrowed','Date Borrowed') !!}
                                {!! Form::date('date_borrowed', null ,['class'=>'form-control','readonly']) !!}
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('date_returned','Date Return') !!}
                                {!! Form::date('date_returned', old('date_returned',  Carbon\Carbon::today()), ['class'=>'form-control date-picker','readonly']) !!}
                            </div>
                        </div>


                        <div class="col-md-2">
                            <div class="form-group">
                                {!! Form::button('Return',['type'=>'submit','class'=>'btn btn-primary']) !!}
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


