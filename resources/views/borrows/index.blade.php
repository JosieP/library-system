@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                @if(Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif

                <div class="panel panel-default">

                    <div class="panel-heading">Student</div><br>
                    <div class="col-md-8">
                        <div class="form-group">
                            {{ link_to_route('borrows.create','Add new borrower',null,['class'=>'btn btn-primary']) }}
                        </div>
                    </div>

                    <div class="panel-body">
                        <table class="table">
                            <tr>
                                <th>Date Borrowed</th>
                                <th>Due Date</th>
                                <th>Student Name</th>
                                <th>Book Name</th>
                                <th>Action</th>
                            </tr>
                            @foreach($borrows as $borrow)
                            <tr>
                                <td>{{ $borrow->date_borrowed }}</td>
                                <td>{{ $borrow->due_date }}</td>
                                <td>{{ $borrow->stud_name }}</td>
                                <td>{{ $borrow->b_name }}</td>
                                <td>{{ link_to_route('borrows.edit','Return',[$borrow->id],['class'=>'btn btn-default']) }}
                                    |
                                    {{ link_to_route('borrows.extend','Extend',[$borrow->id],['class'=>'btn btn-primary']) }}</td>
                            </tr>
                            @endforeach
                        </table>


            </div>
        </div>
    </div>
@endsection
