@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                @if(Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif

                <div class="panel panel-default">

                    <div class="panel-heading">Return Student</div><br>


                    <div class="panel-body">

                        {!! Form::model($borrow,array('route'=>['returns.update',$borrow->id],'method'=>'PUT')) !!}

                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('stud_name','Student Name') !!}
                                {!! Form::text('stud_name', null ,['class'=>'form-control','readonly']) !!}
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('b_name','Book Name') !!}
                                {!! Form::text('b_name', null ,['class'=>'form-control','readonly']) !!}
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('due_date','Date Return') !!}
                                {!! Form::date('due_date', null ,['class'=>'form-control','readonly']) !!}
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('due_date','Date Extend') !!}
                                {!! Form::date('due_date', null ,['class'=>'form-control']) !!}
                            </div>
                        </div>


                        <div class="col-md-2">
                            <div class="form-group">
                                {!! Form::button('Extend',['type'=>'submit','class'=>'btn btn-primary']) !!}
                            </div>
                        </div>
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


