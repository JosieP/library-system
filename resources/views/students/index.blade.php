@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                @if(Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif

                <div class="panel panel-default">

                    <div class="panel-heading">Student</div><br>
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ link_to_route('students.create','Add new student',null,['class'=>'btn btn-primary']) }}

                        </div>
                    </div>

                    <div class="panel-body">

                        <table class="table">
                            <tr>
                                <th>Student ID</th>
                                <th>Student Name</th>
                                <th>Gender</th>
                                <th>Year/Section</th>
                                <th>Action</th>
                            </tr>

                            @foreach($students as $student)
                                <tbody>
                                <tr>
                                    <td>{{ link_to_route('students.show',$student->student_id,$student->id) }}</td>
                                    <td>{{ link_to_route('students.show',$student->name,$student->id) }}</td>
                                    <td>{{ link_to_route('students.show',$student->gender,$student->id) }}</td>
                                    <td>{{ link_to_route('students.show',$student->year,$student->id) }}</td>
                                    <td>
                                        {!! Form::open(array('route'=>['students.destroy',$student->id],'method'=>'DELETE')) !!}
                                        {{ link_to_route('students.edit','Edit',[$student->id],['class'=>'btn btn-primary']) }}
                                        |
                                        {!! Form::button('Delete',['class'=>'btn btn-danger','type'=>'submit']) !!}
                                        {!!  Form::close() !!}
                                    </td>

                                </tr>
                                </tbody>
                            @endforeach
                        </table>


                    </div>
                </div>

            </div>
        </div>
    </div>


@endsection

<script>
    function search() {
        $('form input:text').keyup(function () {
            $value=$(this).val();
            $.ajax({
                type : 'get',
                url : '{{URL::to('search')}}',
                data : {'search':$value},
                success:function (data) {
                    alert(data);
                    //$('tbody').html(data);

                }
            })
        })
    }

</script>
