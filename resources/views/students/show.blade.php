@extends('layouts.app')
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                @if(Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif

                <div class="panel panel-default">

                    <div class="panel-heading">Student</div><br>
                    <div class="panel-body">


                        Student Id : {{ link_to_route('students.show',$student->student_id,$student->id) }}<br>
                        Student First Name : {{ link_to_route('students.show',$student->firstname,$student->id) }}<br>
                        Student Last Name : {{ link_to_route('students.show',$student->lastname,$student->id) }}<br>
                        Student Year/Section : {{ link_to_route('students.show',$student->year,$student->id) }}<br><br>
                        <p><h3>Student Borrowed books:</h3></p>

                                        <table class="table">
                                            <tr>
                                                <th>Book Code</th>
                                                <th>Book Name</th>
                                                <th>Book Description</th>
                                                <th>Book Author</th>
                                                <th>Book Release</th>
                                            </tr>

                                                @foreach ($books as $book)
                                                    <tr>
                                                        <td>{{ link_to_route('books.show',$book->isbn,$book->id) }}</td>
                                                        <td>{{ $book->b_name }}</td>
                                                        <td>{{ $book->b_des }}</td>
                                                        <td>{{ $book->b_aut }}</td>
                                                        <td>{{ $book->b_year }}</td>
                                                        </tr>
                                                @endforeach




                                        </table>


                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
