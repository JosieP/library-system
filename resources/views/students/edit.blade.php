@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">

                    <div class="panel-heading">Student</div><br>
                    <div class="panel-body">
                        {!! Form::model($student,array('route'=>['students.update',$student->id],'method'=>'PUT')) !!}
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        {!! Form::label('student_id','Enter student id') !!}
                                        {!! Form::number('student_id',null,['class'=>'form-control','placeholder'=>'Enter Student Id']) !!}
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('firstname','Enter First Name') !!}
                                        {!! Form::text('firstname',null,['class'=>'form-control','placeholder'=>'Enter First Name']) !!}
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('lastname','Enter Last Name') !!}
                                        {!! Form::text('lastname',null,['class'=>'form-control','placeholder'=>'Enter Last Name']) !!}
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('gender','Gender') !!}
                                        <select class="form-control" selected="{{  $student->gender }}">
                                            <option disabled="disabled">Gender</option>
                                            <option>Male</option>
                                            <option>Female</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('year','Enter student year/section') !!}
                                        {!! Form::text('year',null,['class'=>'form-control','placeholder'=>'Enter Student Year & Section']) !!}
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        {!! Form::button('Add student',['type'=>'submit','class'=>'btn btn-primary']) !!}
                                    </div>
                                </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                    @if($errors->all())
                        <ul class="alert alert-danger">
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
            </div>
        </div>
    </div>
@endsection
