@extends('layouts.app')
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                @if(Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif

                <div class="panel panel-default">

                    <div class="panel-heading">Student</div><br>
                    <div class="panel-body">

                        {!! Form::model($student,array('route'=>['students.update',$student->id],'method'=>'PUT')) !!}
                        <div class="col-xs-12">
                            <div class="form-group">
                                {!! Form::label('stud_id','Enter student id') !!}
                                {!! Form::number('stud_id',null,['class'=>'form-control']) !!}
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <div class="form-group">
                                {!! Form::label('stud_name','Enter student name') !!}
                                {!! Form::text('stud_name',null,['class'=>'form-control']) !!}
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <div class="form-group">
                                {!! Form::label('stud_yr','Enter student year/section') !!}
                                {!! Form::text('stud_yr',null,['class'=>'form-control']) !!}
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                {!! Form::button('Update student',['type'=>'submit','class'=>'btn btn-primary']) !!}
                            </div>
                        </div>
                        {!! Form::close() !!}

                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
