@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                @if(Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif

                <div class="panel panel-default">

                    <div class="panel-heading">Books Returned</div><br>


                    <div class="panel-body">
                        <table class="table">
                            <tr>
                                <th>Date Returned</th>
                                <th>Student ID</th>
                                <th>Student Name</th>
                                <th>ISBN</th>
                                <th>Book Name</th>
                            </tr>

                            @foreach($returns as $return)
                                <tr>
                                    <td>{{ $return->date_returned }}</td>
                                    <td>{{ $return->student_id }}</td>
                                    <td>{{ $return->stud_name }}</td>
                                    <td>{{ $return->b_id }}</td>
                                    <td>{{ $return->b_name }}</td>
                            @endforeach
                        </table>


                    </div>
                </div>
            </div>
@endsection
