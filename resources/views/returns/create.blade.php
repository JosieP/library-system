@extends('layouts.app')
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                @if(Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif

                <div class="panel panel-default">

                    <div class="panel-heading">Student</div><br>
                    <div class="panel-body">

                        <table class="table">
                            @foreach ($students as $student)
                                <td>Student ID : {{ $student->student_id }}<br>
                                    Student First Name : {{ $student->firstname }}<br>
                                    Student Last Name : {{ $student->lastname }}<br>
                                    Student Year/Section : {{ $student->year }}<br>
                            @endforeach

                            @foreach ($books as $book)
                                <td>Book ISBN : {{ $book->isbn }}<br>
                                    Book Name : {{ $book->b_name }}<br>
                                    Book Description : {{ $book->b_des }}<br>
                                    Book Author : {{ $book->b_aut }}<br>
                                    Book Release : {{ $book->b_year }}<br></td>
                                @endforeach
                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
@endsection
